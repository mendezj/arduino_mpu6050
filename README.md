# arduino_mpu6050

This project is meant to show how to retrieve inertial readings from an mpu6050.
I modified the library written by Jeff Rowberg in order to return
the pointer to the raw data to avoid copying into temporary variables.

In the arduino .ino file, I use a timer to write out of the serial every X milliseconds.
I've tested up to 1 Khz which is the maximum resolution allowed by the Timer object.

In the future, I'll test other timer libraries with microsecond accuracy to reach 8 khz for
the accelerometer according to the data sheet.

This effort is mainly to improve dead reckoning calculations in a separate project.

TODO:
Write a function to also return the pointer with magnetometer readings.
