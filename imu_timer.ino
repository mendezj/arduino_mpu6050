#include <Event.h>
#include <Timer.h>
#include <Wire.h>

#include "MPU6050.h"
#include "I2Cdev.h"


long BAUD_RATE = 460800;        // baud rate for the serial connection

int NUM_TX_BYTES = 14;          // Number of bytes to transmit ( accelerometer, gyroscope, and temperature )

int TX_PERIOD = 10;             // the period of the callback function (in milliseconds)
                                // NOTE: at i.e. TX_PERIOD = 1, the function runs every millisecond.
                                // So it'll run 1000 times a second, reaching 1 Khz. This was tested.
                                // This is the maximum it'll run at because of the Timer's object resolution.
                                // If we need to run faster, (ie. - reaching the accelerometers 8 khz)
                                // we need a Timer with accuracy in the order of microseconds.

// The mpu6050 object containing the sensor data retrieval functions
MPU6050 accel_gyro;

// Timer used to send data every X milliseconds
Timer t;

// pointer to hold the data retrieved from the sensor
uint8_t* data;

// callback function. This function is linked to the timer to run periodically
void tx_reading();

void setup()
{
  Serial.begin(BAUD_RATE);                                  // Open the serial communication with the given baud rate
  accel_gyro.initialize();                                  // Initialize the sensor
  accel_gyro.setFullScaleAccelRange(MPU6050_ACCEL_FS_2);    // Set the sensor sensitivity
  t.every(TX_PERIOD, tx_reading, NULL);                            // Set up the timer callback
}

void loop() 
{
  t.update();                                             // The loop just lets the timer handle the updating
}

void tx_reading()
{
  data = accel_gyro.getMotion6_data();                     // Retrieve the data from the sensor  
  
  Serial.write(data, NUM_TX_BYTES);                        // Send the data out of the serial
}
